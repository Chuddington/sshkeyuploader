#!/usr/bin/env bash

# Note: Requires SSHPASS to be installed, which can be built from the following
# Git Repository: https://github.com/kevinburke/sshpass
#
# Creates a backup of the known_hosts file, then replaces it with the public
# keys of the defined server list.  The current user's public key is passed
# to each server.

# create a shortcut for the script to use.  Makes it easier for the user to
# edit and define their own ssh base directory
export SSH_HOME="${HOME}/.ssh"

# Allow duplicates within the server list.  One entry per line
server_list=$(sort -f "./serverList.txt" | uniq)

# Suffix for known_hosts file
date_time_now=$(date '+%Y%m%d%H%M%S')

# location of the known_hosts file that will be backed up and overwritten
known_hosts="${SSH_HOME}/known_hosts"

# location of the public key to be copied over onto the servers
public_key="${SSH_HOME}/id_rsa.pub"

user_name=""
password=""



# Get the User name and password via user input
read -rp 'Username: ' user_name
read -rsp 'Password: ' password

# Backup the old known_hosts.  If the user requires anything from the old file,
# they can grab it
mv "${known_hosts}" "${known_hosts}_${date_time_now}"

# for each unique, sorted line in the serverList.txt file
while read -r server
do
  # remove leading whitespace characters
  server_no_whitespace="${server#"${server%%[![:space:]]*}"}"
  # remove trailing whitespace characters
  server_no_whitespace="${server_no_whitespace%"${server_no_whitespace##*[![:space:]]}"}"

  # if the trimmed server has a length greater than 0
  if [[ -n "${server_no_whitespace}" ]]
  then
    # Obtain the public keys from the server, and append to the known hosts
    # file
    ssh-keyscan "${server_no_whitespace}" >> "${known_hosts}"

    # Copy the user's public key to the server using ssh-copy-id.
    # sshpass is used to provide the user's password and get past the
    # interaction stage of ssh-copy-id
    sshpass -p "${password}" ssh-copy-id -i "${public_key}" "${user_name}@${server_no_whitespace}"
  fi
done <<< "${server_list}"
